<?php

namespace plentific\service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use plentific\interface\UserServiceInterface;
use plentific\model\User;
use Psr\Http\Message\ResponseInterface;
use Exception;

class UserService implements UserServiceInterface
{
    private const API_URL = 'https://reqres.in/api/users';
    private const MAX_RETRIES = 3;
    private Client $client;

    public function __construct(Client $client)
    {
        // Inject the Guzzle client into the service
        $this->client = $client;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function getUser(int $id): User
    {
        // Send the request to the API and get the response body
        $response = $this->sendRequest(self::API_URL . '/' . $id);
        $data = $this->parseResponse($response);

        // Create a User object from the API response
        return new User(
            $data['id'],
            $data['first_name'] . ' ' . $data['last_name'],
            $data['job']
        );
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function getUsers(int $page = 1, int $perPage = 10): array
    {
        // Build the URL with query parameters
        $url = self::API_URL . '?page=' . $page;

        // Send the request to the API and get the response body
        $response = $this->sendRequest($url);
        $data = $this->parseResponse($response);

        $users = [];
        // Create User objects from the API response
        foreach ($data as $userData) {
            $users[] = new User(
                $userData['id'],
                $userData['first_name'] . ' ' . $userData['last_name'],
                $userData['job']
            );
        }

        return $users;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function createUser(string $name, string $job): int
    {
        $url = self::API_URL;
        $data = ['name' => $name, 'job' => $job];
        $response = $this->sendRequest($url, 'POST', json_encode($data));
        $data = $this->parseResponse($response);

        return $data['id'];
    }

    /**
     * @throws GuzzleException
     */
    private function sendRequest(string $url, string $method = 'GET', string $body = null, int $retryCount = 0): ResponseInterface
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ];

        // Add the request body for POST requests
        if ($method === 'POST') {
            $options['body'] = $body;
        }

        try {
            return $this->client->request($method, $url, $options);
        } catch (ClientException $e) {
            // Retry the request if the server returns a 5xx error
            if ($retryCount < self::MAX_RETRIES && $e->getCode() >= 500) {
                // Retry on server errors
                return $this->sendRequest($url, $method, $body, $retryCount + 1);
            }
            throw new Exception("API request failed: " . $e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    private function parseResponse(ResponseInterface $response): array
    {
        // Parse the JSON response
        $data = json_decode($response->getBody(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            // Throw an exception if the response is not valid JSON
            throw new Exception("Error parsing API response");
        }

        return $data['data'] ?? [];
    }
}