<?php

namespace plentific\model;

use JsonSerializable;

class User implements JsonSerializable
{
    public int $id;
    public string $name;
    public string $job;

    public function __construct(int $id, string $name, string $job)
    {
        $this->id = $id;
        $this->name = $name;
        $this->job = $job;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getJob(): string
    {
        return $this->job;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'job' => $this->job
        ];
    }
}