<?php

namespace plentific\interface;
use plentific\model\User;

interface UserServiceInterface
{
    public function getUser(int $id): User;

    public function getUsers(int $page = 1, int $perPage = 10): array;

    public function createUser(string $name, string $job): int;
}