<?php

use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use plentific\model\User;
use plentific\service\UserService;

class UserServiceTest extends TestCase
{

    private $httpClient;

    protected function setUp(): void
    {
        parent::setUp();

        // Create a mock Guzzle client
        $this->httpClient = $this->createMock(Client::class);
    }

    /**
     * @throws GuzzleException
     */
    public function testGetUser()
    {
        // Mock API response
        $userId = 1;
        $responseBody = json_encode([
            'data' => [
                'id' => $userId,
                'first_name' => 'John',
                'last_name' => 'Doe',
                'job' => 'Developer'
            ]
        ]);

        // Set up expectations for the HTTP client mock
        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn(new Response(200, [], $responseBody));

        // Create an instance of UserService with the mock client
        $userService = new UserService($this->httpClient);

        // Call the getUser method
        $user = $userService->getUser($userId);

        // Assert that the method returns a User object with the correct properties
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($userId, $user->getId());
        $this->assertEquals('John Doe', $user->getName());
        $this->assertEquals('Developer', $user->getJob());
    }


    /**
     * @throws GuzzleException
     */
    public function testGetUsers()
    {
        // Mock API response
        $responseBody = json_encode([
            'data' => [
                ['id' => 1, 'first_name' => 'John', 'last_name' => 'Doe', 'job' => 'Developer'],
                ['id' => 2, 'first_name' => 'Jane', 'last_name' => 'Smith', 'job' => 'Designer']
            ]
        ]);

        // Set up expectations for the HTTP client mock
        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn(new Response(200, [], $responseBody));

        // Create an instance of UserService with the mock client
        $userService = new UserService($this->httpClient);

        // Call the getUsers method
        $users = $userService->getUsers();

        // Assert that the method returns an array of User objects
        $this->assertIsArray($users);
        $this->assertCount(2, $users);

        // Assert the properties of the first user
        $this->assertEquals(1, $users[0]->getId());
        $this->assertEquals('John Doe', $users[0]->getName());
        $this->assertEquals('Developer', $users[0]->getJob());

        // Assert the properties of the second user
        $this->assertEquals(2, $users[1]->getId());
        $this->assertEquals('Jane Smith', $users[1]->getName());
        $this->assertEquals('Designer', $users[1]->getJob());
    }


    /**
     * @throws GuzzleException
     */
    public function testCreateUser()
    {
        // Mock API response
        $responseBody = json_encode([
            'data' => [
                'id' => 123,
                'name' => 'John Doe',
                'job' => 'Developer'
            ]
        ]);

        // Set up expectations for the HTTP client mock
        $this->httpClient->expects($this->once())
            ->method('request')
            ->willReturn(new Response(200, [], $responseBody));

        // Create an instance of UserService with the mock client
        $userService = new UserService($this->httpClient);

        // Call the createUser method
        $userId = $userService->createUser('John Doe', 'Developer');

        // Assert that the method returns the ID of the created user
        $this->assertEquals(123, $userId);
    }

}