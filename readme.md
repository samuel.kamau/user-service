# Marathon Runner User Service

Authored by: [Samuel Kamau](mailto:samuelnjengakamau@gmail.com)

## Features

- **Get User**
- **List Users**
- **Create User**

## Installation

```bash
composer install
vendor/bin/phpunit src/tests/UserServiceTest.php