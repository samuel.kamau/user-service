<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'marathon-runner/user-service',
  ),
  'versions' => 
  array (
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.8.1',
      'version' => '7.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41042bc7ab002487b876a0683fc8dce04ddce104',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbff78d96034045e58e13dedd6ad91b5d1253223',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.6.2',
      'version' => '2.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '45b30f99ac27b5ca93cb4831afe16285f57b8221',
    ),
    'marathon-runner/user-service' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7284c22080590fb39f2ffa3e9057f10a4ddd0e0c',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v5.0.2',
      'version' => '5.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '139676794dc1e9231bf7bcd123cfc0c99182cb13',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '54750ef60c58e43759730615a392c31c80e23176',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '10.1.14',
      'version' => '10.1.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3f51450ebffe8e0efdf7346ae966a656f7d5e5b',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a95037b6d9e608ba092da1b23931e537cadc3c3c',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5e568ba02fa5ba0ddd0f618391d5a9ea50b06d7',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c7b06ff49e3d5072f057eb1fa59258bf287a748',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '6.0.0',
      'version' => '6.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2a2d67966e740530f4a3343fe2e030ffdc1161d',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '10.5.13',
      'version' => '10.5.13.0',
      'aliases' => 
      array (
      ),
      'reference' => '20a63fc1c6db29b15da3bd02d4b6cf59900088a7',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '2.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '402d35bcb92c70c026d1a6a9883f06b2ead23d71',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c34583b87e7b7a8055bf6c450c2c77ce32a24084',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a81fee9eef0b7a76af11d121767abc44c104e503',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e3a687f7d8ae33fb362c5c0743794bbb2420a1d',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '5.0.1',
      'version' => '5.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2db5010a484d53ebf536087a70b4a5423c102372',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '68ff824baeae169ec9f2137158ee529584553799',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '5.1.1',
      'version' => '5.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c41e007b4b62af48218231d6c2275e4c9b975b2e',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '6.0.1',
      'version' => '6.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '43c751b41d74f96cbbd4e07b7aec9675651e2951',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '5.1.2',
      'version' => '5.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '955288482d97c19a372d3f31006ab3f37da47adf',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '6.0.2',
      'version' => '6.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '987bafff24ecc4c9ac418cab1145b96dd6e9cbd9',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '856e7f6a75a84e339195d48c556f23be2ebf75d0',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '202d0e344a580d7f7d04b3fafce6933e59dae906',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '24ed13d98130f0e7122df55d06c5c4942a577957',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '5.0.0',
      'version' => '5.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '05909fb5bc7df4c52992396d0116aed689f93712',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '4.0.0',
      'version' => '4.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '462699a16464c3944eefc02ebdd77882bd3925bf',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c51fa83a5d8f43f1402e3f32a005e6262244ef17',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c3aff79d10325257a001fcf92d991f24fc967cf',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '737eda637ed5e28c3413cb1ebe8bb52cbf1ca7a2',
    ),
  ),
);
